% This demo illustrates how to 
% 
% - obtain recurrence coefficients for orthogonal polynomials
% - evaluate univariate orthogonal polynomials
% - evaluate derivatives of univariate orthogonal polynomials
% - compute Gauss quadrature rules
% - generate multi-index sets
% - evaluate multivariate (tensor-product) polynomials

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% - obtain recurrence coefficients for orthogonal polynomials
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Parameters for jacobi family, both > -1
alph = 0;
bet = 0;
N = 20;
[aj,bj] = jacobi_recurrence(N, alph, bet);

% Parameter for Hermite family, > -1
rho = 0;
N = 20;
[ah,bh] = hermite_recurrence(N, rho);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% - evaluate orthogonal polynomials
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

x = linspace(-1, 1, 1e2)';
% With N recurrence coefficients, can only evaluate up to degree N-1 polynomials
Vj = poly_eval(aj, bj, x, N-1); % Jacobi polynomials
Vh = poly_eval(ah, bh, x, N-1); % Hermite polynomials

% Row j of V contains the first N polynomials evaluated at point j
% Column k of V contains the k'th polynomial evaluated at all points

% Plot of the first, 5th, and 9th polynomials (degrees 0, 4, 8)
%plot(x, Vj(:,[1 5 9]));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% - evaluate derivatives of orthogonal polynomials
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

d = 2; % Compute second derivative
Vj_d = poly_eval(aj, bj, x, N-1, d); % Jacobi polynomials
Vh_d = poly_eval(ah, bh, x, N-1, d); % Hermite polynomials

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% - compute Gauss quadrature rules
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Compute M-point quadrature rule
M = 80;

% Requires at least M+1 recurrence coefficients
[aj,bj] = jacobi_recurrence(M+1, alph, bet);
[ah,bh] = hermite_recurrence(M+1, rho);

[xj,wj] = gauss_quadrature(aj,bj,M);
[xh,wh] = gauss_quadrature(ah,bh,M);

% Check that these quadrature rules are accurate:
% M-point Gaussian rules integrate degree-(2M-1) polynomials exactly
% I.e., these should integrate products of orthonormal polynomials
%
%   p_j(x) p_k(x)
%
% exactly when j + k <= 2M-1. We'll test this for j + k <=2M-2:

% All polynomials up to degree M-1
V = poly_eval(aj, bj, xj, M-1);
% Each entry of this matrix is the quadrature rule applied to a product of two
% orthonormal polynomials. Therefore, this matrix should be the identity if the
% quadrature rule is accurate enough.
Gj = V'*diag(wj)*V;
errj = norm(Gj - eye(M));

% All same for Hermite:
V = poly_eval(ah, bh, xh, M-1);
Gh = V'*diag(wh)*V;
errh = norm(Gh - eye(M));

% Both errj and errh should be close to 0

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% - generate multi-index sets
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The most common kind of multi-index set is the total-degree set: the set of
% multi-indices on the d-dimensional lattice of non-negative integers whose
% componentwise sum is less than degree k.
d = 4;
k = 5;

% Each row of lambdas is an individual multi-index
lambdas = total_degree_indices(d,k);

% There is a popular alternative set of indices, the set of hyperbolic cross indices.
% See the comments for hyperbolic_cross_indices.m
lambdas_h = hyperbolic_cross_indices(d,k);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% - evaluate multivariate (tensor-product) polynomials
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% To evaluate multivariate polynomials, you need to generate an index set and
% specify recurrence coefficients.

d = 3;
k = 4;
lambdas = total_degree_indices(d,k);

alph = 0;
bet = 0;
recurrence = @(NN) jacobi_recurrence(NN, alph, bet);

M = 100;
% M random points on the hypercube [-1,1]^d, each row is a point
x = 2*rand([M d]) - 1;

% size(V) is ( M x size(lambdas,1) )
V = mpoly_eval(x, lambdas, recurrence);

% Evaluate d/dy:
Vd = mpoly_eval(x, lambdas, recurrence, [0 1 0]);
